package com.example.andrarchcomponent2.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.andrarchcomponent2.R
import com.example.andrarchcomponent2.model.`object`.Note


class NoteAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var notes: List<Note> = ArrayList<Note>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.note_item, parent, false)
        return NoteHolder(view)
    }

    override fun getItemCount(): Int {
        return notes.size
    }

    fun setData(notes: List<Note>){
        this.notes = notes
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val noteCurrent = notes[position]
        if (holder is NoteHolder){
            holder.textViewTitle?.text = noteCurrent.title
            holder.textViewDescription?.text = noteCurrent.description
            holder.textViewPriority?.text = noteCurrent.priority.toString()
        }
    }

    inner class NoteHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
       var textViewTitle: TextView?=null
       var textViewDescription: TextView?=null
       var textViewPriority: TextView?=null

        init {
            findById()
        }

         private fun findById(){
            textViewTitle = itemView.findViewById(R.id.text_view_title)
            textViewDescription = itemView.findViewById(R.id.text_view_description)
            textViewPriority = itemView.findViewById(R.id.text_view_priority)
        }
    }
}