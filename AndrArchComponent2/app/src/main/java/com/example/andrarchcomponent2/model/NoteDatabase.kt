package com.example.andrarchcomponent2.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.andrarchcomponent2.model.`object`.Note
import com.example.andrarchcomponent2.model.dao.NoteDAO

@Database(entities = [Note::class], version = 1)
abstract class NoteDatabase : RoomDatabase(){
    abstract fun getNoteDao():NoteDAO

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: NoteDatabase? = null

        fun getDatabase(context: Context): NoteDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    NoteDatabase::class.java,
                    "note_table"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }

//    companion object{
//        private lateinit var INSTANCE:NoteDatabase
//
//        fun getInstance(context: Context):NoteDatabase{
//            if(!Companion::INSTANCE.isInitialized){
//                INSTANCE = Room.databaseBuilder(context.applicationContext, NoteDatabase::class.java, "note_table")
//                    .allowMainThreadQueries()
//                    .fallbackToDestructiveMigration()
//                    .build()
//            }
//            return INSTANCE
//        }
//    }
}