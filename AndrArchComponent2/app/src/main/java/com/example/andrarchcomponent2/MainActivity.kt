package com.example.andrarchcomponent2

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.andrarchcomponent2.adapter.NoteAdapter
import com.example.andrarchcomponent2.viewModel.NoteViewModel
import kotlinx.android.synthetic.main.activity_main.*
import java.util.Arrays.asList
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.example.andrarchcomponent2.model.`object`.Note
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var noteViewModel: NoteViewModel
    private lateinit var noteAdapter: NoteAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recycler_view.layoutManager = LinearLayoutManager(this)
        noteAdapter = NoteAdapter()
        recycler_view.setHasFixedSize(true)
        recycler_view.adapter = noteAdapter

        noteViewModel = ViewModelProviders.of(this).get(NoteViewModel::class.java)

        noteViewModel.allNotes.observe(this, Observer {notes->
            notes?.let { noteAdapter.setData(it)}
            Toast.makeText(this, "hello",Toast.LENGTH_SHORT).show()
            Log.i("size = ", notes.size.toString())
        })
    }

    private fun getNotes(): Collection<Note> {
        return Arrays.asList(
            Note(
                1, "Title 1","desc 1", 1
            ),
            Note(
                2, "Title 2","desc 2", 2
            )
        )
    }
}
