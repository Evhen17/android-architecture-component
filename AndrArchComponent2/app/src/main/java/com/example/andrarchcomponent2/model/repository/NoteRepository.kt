package com.example.andrarchcomponent2.model.repository

import androidx.lifecycle.LiveData
import com.example.andrarchcomponent2.model.NoteDatabase
import com.example.andrarchcomponent2.model.`object`.Note
import com.example.andrarchcomponent2.model.dao.NoteDAO

class NoteRepository(private val noteDAO: NoteDAO):NoteRepositoryInterface {
    override fun insert(note: Note) {
        noteDAO.insert(note)
    }

    override fun update(note: Note) {
        noteDAO.update(note)
    }

    override fun delete(note: Note) {
        noteDAO.delete(note)
    }

    override fun deleteAllNotes() {
        noteDAO.deleteAllNotes()
    }

    override fun getAllNotes(): LiveData<List<Note>> {
        return noteDAO.getAllNotes()
    }
}