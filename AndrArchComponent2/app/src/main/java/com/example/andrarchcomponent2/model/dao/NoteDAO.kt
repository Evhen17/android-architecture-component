package com.example.andrarchcomponent2.model.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.andrarchcomponent2.model.`object`.Note

@Dao
interface NoteDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(note:Note)

    @Update
    fun update(note: Note)

    @Delete
    fun delete(note: Note)

    @Query("Delete from note_table")
    fun deleteAllNotes()

    @Query("Select * from note_table order by priority asc")
    fun getAllNotes(): LiveData<List<Note>>
}