package com.example.andrarchcomponent2.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.andrarchcomponent2.model.NoteDatabase
import com.example.andrarchcomponent2.model.`object`.Note
import com.example.andrarchcomponent2.model.repository.NoteRepository


class NoteViewModel(application: Application) : AndroidViewModel(application) {
    private val noteRepository: NoteRepository
    val allNotes:LiveData<List<Note>>

    init {
        val noteDao = NoteDatabase.getDatabase(application).getNoteDao()
        noteRepository = NoteRepository(noteDao)
        allNotes = noteRepository.getAllNotes()
    }

    fun insert(note: Note) {
        noteRepository.insert(note)
    }

    fun update(note: Note) {
        noteRepository.update(note)
    }

    fun delete(note: Note) {
        noteRepository.delete(note)
    }

    fun deleteAllNotes() {
        noteRepository.deleteAllNotes()
    }

}