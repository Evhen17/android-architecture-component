package com.example.andrarchcomponent2.model.repository

import androidx.lifecycle.LiveData
import com.example.andrarchcomponent2.model.`object`.Note

interface NoteRepositoryInterface {
    fun insert(note:Note)

    fun update(note:Note)

    fun delete(note:Note)

    fun deleteAllNotes()

    fun getAllNotes(): LiveData<List<Note>>
}