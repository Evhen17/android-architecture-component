package com.example.andrarchcomponent2.model.`object`

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity (tableName = "note_table")
data class Note (
    @PrimaryKey
    var id:Int,
    var title:String,
    var description: String,
    var priority: Int)

